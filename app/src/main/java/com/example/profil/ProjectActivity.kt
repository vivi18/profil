package com.example.profil

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ProjectActivity : AppCompatActivity() {

    lateinit var projectView: RecyclerView
    lateinit var projectAdapter: ProjectAdapter
    val  list = ArrayList<ProjectData>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project)

        projectView = findViewById(R.id.rvProject)
        projectView.layoutManager = LinearLayoutManager(this)

        list.add(ProjectData("UPB_XIPPLG2_3035","Aplikasi android","https://gitlab.com/vivi18/upb_xipplg2_3035")
        )


        list.add(ProjectData("Profil Guru","Aplikasi android","https://gitlab.com/vivi18/profil-guru11")
        )

        projectAdapter = ProjectAdapter(list)
        projectView.adapter = projectAdapter
    }
}